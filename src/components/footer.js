import React from "react";

const Footer = () => {
  return (
    <div className="footer-container">
      <div className="footer-wrapper">
        <div className="get-in-touch-wrapper">
          <div className="info-wrapper">Get in touch</div>
          <div className="email-component">name</div>
        </div>
        <div className="copyright-wrapper">copyright</div>
      </div>
    </div>
  );
};

export default Footer;
