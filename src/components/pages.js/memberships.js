import React from "react";

const Memberships = () => {

  return (
    <div className="memberships-container">
      <div className="img-container">
        <div className="memberships-img">
          <div className="header-wrapper">
            <h1 className="memberships-img-header header1">GET YOUR GYM</h1>
            <h1 className="memberships-img-header">MEMBERSHIP</h1>
            <h1 className="memberships-img-header">TODAY!</h1>
          </div>
        </div>
      </div>
      <div className="memberships-info-container">
        <p className="info">
          Are you ready to join a local gym that you can call home? Our
          Membership Cost breakdown/options are as follows: $25 Membership Set
          Up Fee Month to Month - $69/month 3 Month Contract - $59/month 6 Month
          Contract - $49/month 1 Year Membership paid up front - $480 6 Month
          Membership paid up front - $270 3 Month Membership paid up front -
          $160 Single Day Drop in $10/day ($5 on Sunday) Additional
          Costs/Options: Interested in personal training and a gym membership?
          If you purchase a training package of $200 or more each month with a
          trainer, you'll receive a discounted membership rate of $30/month! If
          you bring/refer a friend you will receive a $25 credit to your
          account. ​ We do not have a manager on site at all times. Instead, we
          set up appointments to meet you at the gym, show you around and if
          you'd like to set up your membership, we will do that too! ​ See you
          at BMB! ​
        </p>
      </div>
    </div>
  );

};

export default Memberships;
