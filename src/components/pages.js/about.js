import React from "react";
import TourVid from "./about-components/tour-vid";

const About = () => {
  return (
    <div className="about-container">
      <TourVid />
    </div>
  );
};

export default About;
